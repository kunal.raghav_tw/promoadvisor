import React, {Component} from "react";
import {Button} from "reactstrap";


export class Promotions extends Component {

    constructor(props) {
        super(props);
        this.state = {promotions: [], loading: true}

        this.getPromotionsFromDB = this.getPromotionsFromDB.bind(this)
    }

    renderForecastsTable(promotions) {
        return (
            <table className='table table-striped' aria-labelledby="tabelLabel">
                <thead>
                <tr>
                    <th>Promotion Name</th>
                    <th>Item Name</th>
                    <th>Strategy</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                {promotions.map(promotion => {
                        return (
                            <tr key={promotion.id}>
                                <td>{promotion.promotionName}</td>
                                <td>{promotion.itemName}</td>
                                <td>{promotion.tactic}</td>
                                <td>
                                    <Button className={"btn-success"}>Edit</Button>
                                    <span style={{marginLeft: "20px"}}/>
                                    <Button className={"btn-danger"} onClick={()=>{this.deletePromotion(promotion.id)}}>Delete</Button>
                                </td>
                            </tr>)
                    }
                )}
                </tbody>
            </table>
        );
    }

    render() {
        return (
            <div>
                <h1>Promotions</h1>
                {this.state.loading ? "Loading ..." : this.renderForecastsTable(this.state.promotions)}
            </div>
        )
    }

    componentDidMount() {
        this.getPromotionsFromDB()
    }

    async getPromotionsFromDB() {
        this.setState({loading: true})
        // await sleep(1000)
        await fetch('/promotion')
            .then(res => res.json())
            .then(data => this.setState({promotions: data, loading: false}))
    }

    deletePromotion(id){
        var requestOptions = {
            method: 'POST',
            redirect: 'follow'
        };

        fetch(`http://localhost:50000/promotion/delete?id=${id}`, requestOptions)
            .then(response => response.text())
            .then(result => {
                console.log(result)
                this.getPromotionsFromDB()
            })
            .catch(error => console.log('error', error));
    }
}

export function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}