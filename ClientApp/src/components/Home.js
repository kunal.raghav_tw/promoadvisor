import React, {Component} from 'react';
import {Button, Card, CardBody, Col, Container, Form, FormGroup, Input, Label} from "reactstrap";

export class Home extends Component {
    static displayName = Home.name;

    constructor(props) {
        super(props);
        this.state = {
            promotion: {PromotionName: "", ItemName: "", Tactic: ""},
            items: [],
            loading: true,
            addedPromotion: false
        }
        this.dropdownWithItems = this.dropdownWithItems.bind(this)
    }


    componentDidMount() {
        this.getItemsFromDB()
    }

    async getItemsFromDB() {
        // await sleep(1000)
        await fetch('/item')
            .then(res => res.json())
            .then(data => {
                this.setState(prev => (
                        {
                            items: data,
                            loading: false,
                            promotions: {
                                ...prev.promotion,
                                ItemName: data[0].itemName
                            }
                        }
                    )
                )
                let name = data[0].itemName;
                this.setState(prev => ({promotion: {...prev.promotion, ItemName: name}}))
            })
    }

    form() {
        return (
            <Container>
                <Card>
                    <CardBody>
                        <Form>
                            <FormGroup row={true}>
                                <Label for="promoName" sm={2}>
                                    Promotion Name
                                </Label>
                                <Col sm={10}>
                                    <Input
                                        id="promoName"
                                        name="promoName"
                                        placeholder="Enter Promotion Name"
                                        value={this.state.promotion.PromotionName}
                                        onChange={(evt) => {
                                            let value = evt.target.value
                                            this.setState(prev => ({
                                                promotion: {
                                                    ...prev.promotion,
                                                    PromotionName: value
                                                }
                                            }))
                                            console.log(value)
                                        }}
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup row={true}>
                                <Label for="ItemName" sm={2}>
                                    Item Name
                                </Label>

                                <Col sm={10}>
                                    {this.state.loading ? "Loading...." : this.dropdownWithItems(this.state.items)}

                                </Col>
                            </FormGroup>
                            <FormGroup row={true}>
                                <Label for="tactic" sm={2}>
                                    Tactic
                                </Label>
                                <Col sm={10}>
                                    <Input
                                        id="tactic"
                                        name="tactic"
                                        placeholder="Enter Tactic Here"
                                        value={this.state.promotion.Tactic}
                                        onChange={(evt) => {
                                            let value = evt.target.value
                                            this.setState(prev => ({
                                                promotion: {
                                                    ...prev.promotion,
                                                    Tactic: value
                                                }
                                            }))
                                        }}
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup row={true}>
                                <Col
                                    sm={{
                                        offset: 2,
                                        size: 10
                                    }}
                                >
                                    <Button className={"btn-success"} onClick={() => {
                                        var myHeaders = new Headers();
                                        myHeaders.append("Content-Type", "application/json");

                                        var raw = JSON.stringify(this.state.promotion);

                                        var requestOptions = {
                                            method: 'POST',
                                            headers: myHeaders,
                                            body: raw,
                                            redirect: 'follow'
                                        };

                                        fetch("/promotion/add", requestOptions)
                                            .then(response => response.text())
                                            .then(result => {
                                                console.log(result)
                                                this.setState(prev =>(
                                                    {
                                                        addedPromotion: true,
                                                        promotion:{...prev.promotion, PromotionName:"", Tactic:""}
                                                    }))
                                            })
                                            .catch(error => console.log('error', error));
                                    }}>
                                        Create
                                    </Button>
                                </Col>
                            </FormGroup>
                        </Form>
                    </CardBody>
                </Card>
            </Container>
        )
    }


    dropdownWithItems(items) {
        return (
            <Input
                id="ItemName"
                name="ItemName"
                type={"select"}
                onChange={(evt) => {
                    let value = evt.target.value
                    this.setState(prev => ({
                        promotion: {
                            ...prev.promotion,
                            ItemName: value
                        }
                    }))
                }}
            >
                {items.map(item =>
                    <option key={item.id}>{item.itemName}</option>
                )}
            </Input>
        )
    }

    messagePrompt() {
        if(this.state.addedPromotion) {
            setTimeout(()=>{this.setState({addedPromotion:false})}, 1000)
            return (
                <div style={{position: "absolute", top: "20px", left: "0px", right: "0px", zIndex: 9}}
                     className={"alert-success alert w-auto"}>
                    Added Promotion Successfully
                </div>
            )
        }else{
            return (React.Fragment)
        }
    }


    render() {
        return (
            <div style={{position: "relative"}}>
                {this.messagePrompt()}
                <h1>Create a new promotion</h1>
                {this.form()}
            </div>
        );
    }
}
