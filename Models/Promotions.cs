using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PromoAdvisor.Models
{
    [Table("PromotionsTable")]
    public class Promotion
    {
        [Key] public int Id { get; set; }

        public string PromotionName { get; set; }
        public string ItemName { get; set; }
        public string Tactic { get; set; }
    }
}