using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PromoAdvisor.Models
{
    [Table("ItemsTable")]
    public class Item
    {
        [Key] public int Id { get; set; }

        public string ItemName { get; set; }
    }
}