using Microsoft.EntityFrameworkCore;
using PromoAdvisor.Models;

namespace PromoAdvisor.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Item> Items { get; set; }
        public DbSet<Promotion> Promotions { get; set; }
    }
}