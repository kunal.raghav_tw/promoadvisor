﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PromoAdvisor.Migrations
{
    public partial class addItemColumnToPromotionTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ItemName",
                table: "PromotionsTable",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ItemName",
                table: "PromotionsTable");
        }
    }
}
