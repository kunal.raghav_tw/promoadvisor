﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PromoAdvisor.Migrations
{
    public partial class renameTableToItemsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_PromoItems",
                table: "PromoItems");

            migrationBuilder.RenameTable(
                name: "PromoItems",
                newName: "ItemsTable");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ItemsTable",
                table: "ItemsTable",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ItemsTable",
                table: "ItemsTable");

            migrationBuilder.RenameTable(
                name: "ItemsTable",
                newName: "PromoItems");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PromoItems",
                table: "PromoItems",
                column: "Id");
        }
    }
}
