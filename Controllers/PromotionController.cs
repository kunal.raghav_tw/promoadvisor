using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using PromoAdvisor.Data;
using PromoAdvisor.Models;

namespace PromoAdvisor.Controllers
{
    public class PromotionController : Controller
    {
        private readonly ApplicationDbContext _dbContext;

        public PromotionController(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        // GET
        public IEnumerable<Promotion> Index()
        {
            return _dbContext.Promotions;
        }

        [HttpPost]
        public IActionResult Add([FromBody] Promotion promotion)
        {
            if (promotion is null) return BadRequest(new ArgumentNullException());
            Console.WriteLine($"{promotion.Tactic} {promotion.ItemName} {promotion.PromotionName}");
            _dbContext.Promotions.Add(promotion);
            _dbContext.SaveChanges();
            return Created(new Uri("/Promotion/Index", UriKind.Relative), promotion);
        }


        [HttpPost]
        public IActionResult Update([FromBody] Promotion promotion)
        {
            if (promotion is null) return BadRequest(new ArgumentNullException());

            _dbContext.Promotions.Update(promotion);
            _dbContext.SaveChanges();
            return Accepted(promotion);
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            if (_dbContext.Promotions.Find(id) is null) return NotFound();
            var promotion = _dbContext.Promotions.Find(id);
            _dbContext.Promotions.Remove(promotion);
            _dbContext.SaveChanges();
            return Ok();
        }
    }
}