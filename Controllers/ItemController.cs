using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using PromoAdvisor.Data;
using PromoAdvisor.Models;

namespace PromoAdvisor.Controllers
{
    public class ItemController : Controller
    {
        private readonly ApplicationDbContext _db;

        public ItemController(ApplicationDbContext db)
        {
            _db = db;
        }

        // GET
        [HttpGet]
        public List<Item> Index()
        {
            IEnumerable<Item> dbPromoItems = _db.Items;
            return dbPromoItems.ToList();
        }
    }
}